angular.module('starter.controllers', [])

.controller('AppCtrl', function ($scope, $ionicModal, $timeout,$ionicLoading) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal

})

.controller('PlaylistsCtrl', function ($scope, $state) {
    $scope.playlists = [

        {
            title: 'Films',
            id: 7
        },
        {
            title: 'Planets',
            id: 8
        },
        {
            title: 'People',
            id: 9
        }
  ];
    $scope.routeMe = function (item) {
        //        $state.go('playlist');

        if (item.id === 7)

        {
            $state.transitionTo('Films');
        }
        if (item.id === 8) {
            $state.transitionTo('Planets');

        }
        if (item.id === 9) {
            $state.transitionTo('People');
        }

    }
})

.controller('People', function ($scope, films,$ionicLoading) {

    $scope.search = function (data) {
//        console.log(data)
        films.getPeople(data).success(function (result) {
            

              if(result.count===0)
                  {    
                       $scope.people = {};
                      $scope.noData=true;
                  
                  }
            else{
             $scope.noData=false;
            $scope.people = result;
            console.log(result);
                            }
        });
    }




})

.controller('Planets', function ($scope, films,$ionicLoading) {
        
     $ionicLoading.show({
      template: '<p>Loading...</p><ion-spinner></ion-spinner>'
    });
    films.getPlanets().success(function (data,dataReady) {
          
//        $scope.dataReady=true,
//        dataReady= !dataReady;
//        console.log(dataReady);
        $scope.planets = data;
    });

})

.controller('Films', function ($scope, films,$ionicLoading) {
    $ionicLoading.show({
      template: '<p>Loading...</p><ion-spinner></ion-spinner>'
    });
    films.getFilms().success(function (data) {
        $scope.filmsbundle = data;
         $ionicLoading.hide();
    });


});


//.controller('PlaylistCtrl', function ($scope, $stateParams, $state) {
//    if($stateParams.genre === null || $stateParams.genre === undefined){
//        //if the state is entered without any parameters, i send the person back to the playlists state
//        
//        $state.go('playlists')
//    }
//    console.log($stateParams)
//    $scope.genre = $stateParams.genre;
//
//});