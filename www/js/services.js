//
//
//
//angular.module('servicemod',[]).factory('films',  function($http) { 
//  return $http.get('http://swapi.co/api/films/') 
//            .success(function(data) { 
//              return data; 
//            }) 
//            .error(function(err) { 
//              return err; 
//            }); 
//} )
//
//
//

angular
    .module('servicemod',[])
    .factory('films',  function($http) { 
      return {
            getFilms: getFilms,
            getPlanets: getPlanets,
            getPeople: getPeople
            //this is a list of different functions i your services file. This allows you to have various functions per service file. In your controller, you would access the various functions by films.getFilms or films.getPlanets 
      };
      
      
      function getFilms() {
       return   $http.get('http://swapi.co/api/films/') 
                .success(function(data) { 
                    return data; 
                }) 
                .error(function(err) { 
                    return err; 
                }); 
      }
      
      function getPlanets (){
          
               return   $http.get('http://swapi.co/api/planets/') 
                .success(function(data) { 
                    return data; 
                }) 
                .error(function(err) { 
                    return err; 
                }); 
        
      }
    
          function getPeople (searchString){
              var url ='http://swapi.co/api/people/?search=' + searchString;
          
               return   $http.get(url) 
                .success(function(data) { 
                    return data; 
                }) 
                .error(function(err) { 
                    return err; 
                }); 
        
      }
      
      
      
    } )